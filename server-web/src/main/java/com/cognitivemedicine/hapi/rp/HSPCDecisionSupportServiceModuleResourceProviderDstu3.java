package com.cognitivemedicine.hapi.rp;

import java.util.List;

import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.GuidanceResponse;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Person;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IBaseResource;

import com.cognitivemedicine.hl7.evaluate.lib.EvaluateOperation;
import com.cognitivemedicine.hl7.evaluate.lib.dstu3.FHIREvaluateOperation;
import com.cognitivemedicine.hl7.evaluate.vs.impl.SimpleCodeMapBasedValueSetResolver;

import ca.uhn.fhir.jpa.rp.dstu3.DecisionSupportServiceModuleResourceProvider;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.OperationParam;
import ca.uhn.fhir.rest.method.RequestDetails;

/**
 * Created by cnanjo on 5/2/16.
 */
public class HSPCDecisionSupportServiceModuleResourceProviderDstu3 extends DecisionSupportServiceModuleResourceProvider {
    
    /**
     * Added by CN for $evaluate operation
     */
    @Operation(name = "$evaluate", idempotent = false, returnParameters = {
            @OperationParam(name = "return", type = GuidanceResponse.class) })
    public GuidanceResponse evaluateOperation(@IdParam IdType theDecisionSupportRuleId,
                                              @OperationParam(name = "requestId") CodeType theRequestId,
                                              @OperationParam(name = "evaluateAtDateTime") List<DateTimeType> theEvaluateAtDateTime,
                                              @OperationParam(name = "inputParameters") Parameters theInputParameters,
                                              @OperationParam(name = "inputData") List<IBaseResource> theInputData,
                                              @OperationParam(name = "patient") Patient thePatient,
                                              @OperationParam(name = "encounter") Encounter theEncounter,
                                              @OperationParam(name = "initiatingOrganization") Organization theOrganization,
                                              @OperationParam(name = "initiatingPerson") Person thePerson,
                                              @OperationParam(name = "userType") CodeableConcept theUserType,
                                              @OperationParam(name = "userLanguage") CodeableConcept theUserLanguage,
                                              @OperationParam(name = "userTaskContext") CodeableConcept theUserTaskContext,
                                              @OperationParam(name = "receivingOrganization") Organization receivingOrganization,
                                              @OperationParam(name = "receivingPerson") Person theReceivingPerson,
                                              @OperationParam(name = "recipientType") CodeableConcept theRecipientType,
                                              @OperationParam(name = "recipientLanguage") CodeableConcept theRecipientLanguage,
                                              @OperationParam(name = "setting") CodeableConcept theSetting,
                                              @OperationParam(name = "settingContext") CodeableConcept theSettingContext,
                                              RequestDetails theRequestDetails) {
        
        if (theInputParameters != null) {
            theInputParameters = this.processParametersReferences(theInputParameters,
                theRequestDetails.getServerBaseForRequest());
        }
        
        Parameters params = new Parameters();
        params.addParameter().setName("requestId").setValue(theRequestId);
        params.addParameter().setName("patient").setResource(thePatient);
        params.addParameter().setName("encounter").setResource(theEncounter);
        if (theInputData != null) {
            for (IBaseResource resource : theInputData) {
                params.addParameter().setName("inputData").setResource((Resource) resource);
            }
        }
        params.addParameter().setName("inputParameters").setResource(theInputParameters);
        
        //TODO: use a real Terminology Server here!
        SimpleCodeMapBasedValueSetResolver.addMapping("2.16.840.1.113883.3.117.1.7.1.35",
            "2.16.840.1.113883.3.117.1.7.1.35");
        SimpleCodeMapBasedValueSetResolver.addMapping("Breastfeeding Readiness Assessment",
            "Breastfeeding Readiness Assessment");
        SimpleCodeMapBasedValueSetResolver.addMapping("Feeding Intention-Not-To-Breastfeed",
            "Feeding Intention-Not-To-Breastfeed");
        
        EvaluateOperation.setValueSetResolverClass(SimpleCodeMapBasedValueSetResolver.class);
        
        FHIREvaluateOperation eval = new FHIREvaluateOperation();
        GuidanceResponse retVal = eval.evaluate(theDecisionSupportRuleId.getIdPart(), params);
        
        return retVal;
    }
    
    private Parameters processParametersReferences(Parameters orig, String serverBase) {
        if (orig == null) {
            return null;
        }
        
        Parameters newParameters = new Parameters();
        for (Parameters.ParametersParameterComponent pc : orig.getParameter()) {
            if (pc.hasValue() && pc.getValue() instanceof Reference) {
                Reference r = (Reference) pc.getValue();
                
                String[] parts = r.getReference().split("/");
                
                IBaseResource resource = this.getContext().newRestfulGenericClient(serverBase).read().resource(parts[0])
                        .withId(parts[1]).execute();
                
                Parameters.ParametersParameterComponent p = new Parameters.ParametersParameterComponent(pc.getNameElement());
                p.setResource((Resource) resource);
                newParameters.addParameter(p);
            } else {
                newParameters.addParameter(pc);
            }
        }
        
        return newParameters;
    }
}
