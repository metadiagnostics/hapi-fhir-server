package com.cognitivemedicine.hapi.rp;

import ca.uhn.fhir.jpa.rp.dstu3.PatientResourceProvider;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.method.RequestDetails;
import com.cognitivemecidine.hapi.eps.dstu3.Context;
import com.cognitivemecidine.hapi.eps.dstu3.PublicationScopeEnum;
import com.cognitivemecidine.hapi.eps.dstu3.SimpleResourcePublisherRegistry;
import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IIdType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cnanjo on 4/29/16.
 */
public class HSPCPatientResourceProvider extends PatientResourceProvider {

    @Autowired
    private SimpleResourcePublisherRegistry resourcePublisherRegistry;

    @Override
    public Patient read(HttpServletRequest theRequest, IIdType theId, RequestDetails theRequestDetails) {
        Patient patient = super.read(theRequest, theId, theRequestDetails);
        resourcePublisherRegistry.publishResource(new Context(RestOperationTypeEnum.READ, PublicationScopeEnum.RESOURCE, Enumerations.ResourceType.PATIENT),patient);
        return patient;
    }

    @Override
    public MethodOutcome create(HttpServletRequest theRequest,
                               Patient thePatient,
                               String theConditional,
                               RequestDetails theRequestDetails) {
        MethodOutcome outcome = super.create(theRequest, thePatient, theConditional, theRequestDetails);
        if(outcome.getResource() != null) {
            resourcePublisherRegistry.publishResource(new Context(RestOperationTypeEnum.CREATE, PublicationScopeEnum.RESOURCE, Enumerations.ResourceType.PATIENT), (DomainResource)outcome.getResource());
        } else {
            resourcePublisherRegistry.publishResourceId(new Context(RestOperationTypeEnum.CREATE, PublicationScopeEnum.RESOURCE, Enumerations.ResourceType.PATIENT), outcome.getId());
        }
        return outcome;
    }


}
