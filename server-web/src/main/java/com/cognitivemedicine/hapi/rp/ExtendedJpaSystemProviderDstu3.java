package com.cognitivemedicine.hapi.rp;

import ca.uhn.fhir.jpa.provider.dstu3.JpaSystemProviderDstu3;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.OperationParam;
import ca.uhn.fhir.rest.method.RequestDetails;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.List;

/**
 * Created by cnanjo on 5/2/16.
 */
public class ExtendedJpaSystemProviderDstu3 extends JpaSystemProviderDstu3 {

    /**
     * Added by CN for $evaluate operation
     */
    @Operation(name="$evaluate", idempotent=false, returnParameters= {
            @OperationParam(name="return", type=GuidanceResponse.class)
    })
    public GuidanceResponse evaluateOperation(
            @OperationParam(name="requestId") CodeType theRequestId,
            @OperationParam(name="evaluateAtDateTime") List<DateTimeType> theEvaluateAtDateTime,
            @OperationParam(name="inputParameters") Parameters theInputParameters,
            @OperationParam(name="inputData") List<IBaseResource> theInputData,
            @OperationParam(name="patient") Patient thePatient,
            @OperationParam(name="encounter") Encounter theEncounter,
            @OperationParam(name="initiatingOrganization") Organization theOrganization,
            @OperationParam(name="initiatingPerson") Person thePerson,
            @OperationParam(name="userType") CodeableConcept theUserType,
            @OperationParam(name="userLanguage") CodeableConcept theUserLanguage,
            @OperationParam(name="userTaskContext") CodeableConcept theUserTaskContext,
            @OperationParam(name="receivingOrganization") Organization receivingOrganization,
            @OperationParam(name="receivingPerson") Person theReceivingPerson,
            @OperationParam(name="recipientType") CodeableConcept theRecipientType,
            @OperationParam(name="recipientLanguage") CodeableConcept theRecipientLanguage,
            @OperationParam(name="setting") CodeableConcept theSetting,
            @OperationParam(name="settingContext") CodeableConcept theSettingContext,
            RequestDetails theRequestDetails) {

        GuidanceResponse retVal = new GuidanceResponse();
        retVal.setRequestId("523495743234");
//		Parameters parameters = new Parameters();
//		parameters.addParameter().setName("return").setValue(new StringType());
        return retVal;
    }
}
