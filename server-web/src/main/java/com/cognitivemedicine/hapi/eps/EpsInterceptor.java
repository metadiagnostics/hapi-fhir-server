package com.cognitivemedicine.hapi.eps;

import ca.uhn.fhir.jpa.entity.ResourceTable;
import ca.uhn.fhir.jpa.interceptor.JpaServerInterceptorAdapter;
import com.cognitivemecidine.hapi.eps.dstu3.SimpleResourcePublisherRegistry;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by cnanjo on 5/9/16.
 */
public class EpsInterceptor extends JpaServerInterceptorAdapter {

    private SimpleResourcePublisherRegistry resourcePublisherRegistry;


    public SimpleResourcePublisherRegistry getResourcePublisherRegistry() {
        return resourcePublisherRegistry;
    }

    public void setResourcePublisherRegistry(SimpleResourcePublisherRegistry resourcePublisherRegistry) {
        this.resourcePublisherRegistry = resourcePublisherRegistry;
    }

    @Override
    public void resourceCreated(ActionRequestDetails theDetails, ResourceTable theResourceTable) {
        resourcePublisherRegistry.triggerPublicationForAnyResources(theDetails, theResourceTable);
        System.out.println("RESOURCE CREATED");
    }

    @Override
    public void resourceUpdated(ActionRequestDetails theDetails, ResourceTable theResourceTable) {
        resourcePublisherRegistry.triggerPublicationForAnyResources(theDetails, theResourceTable);
        System.out.println("RESOURCE UPDATED");
    }

    @Override
    public void resourceDeleted(ActionRequestDetails theDetails, ResourceTable theResourceTable) {
        resourcePublisherRegistry.triggerPublicationForAnyResources(theDetails, theResourceTable);
        System.out.println("RESOURCE DELETED");
    }
}
