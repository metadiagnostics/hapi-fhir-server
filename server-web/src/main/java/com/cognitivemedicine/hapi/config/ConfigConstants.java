/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognitivemedicine.hapi.config;

/**
 *
 * @author esteban
 */
public class ConfigConstants {
 
    public static final String CONFIG_CONTEXT_NAME = "server-web";
    
    public static final String KEY_BASIC_AUTH_ENABLED = "basic.auth.enabled";
    
    public static final String KEY_EPS_ENDPOINT = "eps.endpoint";

    public static final String KEY_EPS_REGISTER_ALL_PUBLISHERS = "eps.register-all-publishers";

    public static final String KEY_EPS_REGISTER_RESOURCE_SPECIFIC_PUBLISHERS = "eps.register-resource-specific-publishers";
    
}
