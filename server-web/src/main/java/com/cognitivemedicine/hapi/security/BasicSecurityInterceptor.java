/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognitivemedicine.hapi.security;

import ca.uhn.fhir.rest.method.RequestDetails;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.hapi.config.ConfigConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author esteban
 */
public class BasicSecurityInterceptor extends InterceptorAdapter {

    private final boolean enabled;
    
    public BasicSecurityInterceptor() {
        ConfigUtils cfg = ConfigUtils.getInstance(ConfigConstants.CONFIG_CONTEXT_NAME);
        this.enabled = cfg.getBoolean(ConfigConstants.KEY_BASIC_AUTH_ENABLED, false);
    }
    

    /**
     * This interceptor implements HTTP Basic Auth, which specifies that a
     * username and password are provided in a header called Authorization.
     */
    @Override
    public boolean incomingRequestPostProcessed(RequestDetails theRequestDetails, HttpServletRequest theRequest, HttpServletResponse theResponse) throws AuthenticationException {
        
        if(!this.enabled){
            return true;
        }
        
        String authHeader = theRequest.getHeader("Authorization");

        // The format of the header must be:
        // Authorization: Basic [base64 of username:password]
        if (authHeader == null || authHeader.startsWith("Basic ") == false) {
            String contextPath = theRequest.getContextPath();
            throw new AuthenticationException("Missing or invalid Authorization header").addAuthenticateHeaderForRealm(contextPath);
        }

        String base64 = authHeader.substring("Basic ".length());
        String base64decoded = new String(Base64.decodeBase64(base64));
        String[] parts = base64decoded.split("\\:");

        String username = parts[0];
        String password = parts[1];

        /*
       * Here we test for a hardcoded username & password. This is
       * not typically how you would implement this in a production
       * system of course..
         */
        if (!username.equals("connectathon") || !password.equals("somePa$$w0rd")) {
            String contextPath = theRequest.getContextPath();
            throw new AuthenticationException("Invalid username or password").addAuthenticateHeaderForRealm(contextPath);
        }

        // Return true to allow the request to proceed
        return true;
    }

}
