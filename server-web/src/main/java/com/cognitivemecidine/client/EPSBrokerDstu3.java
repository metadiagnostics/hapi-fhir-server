package com.cognitivemecidine.client;

import ca.uhn.fhir.context.FhirContext;
import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.socraticgrid.hl7.services.eps.accessclients.brokermanagement.BrokerManagementServiceSE;
import org.socraticgrid.hl7.services.eps.accessclients.publication.PublicationServiceSE;
import org.socraticgrid.hl7.services.eps.accessclients.subscription.SubscriptionServiceSE;
import org.socraticgrid.hl7.services.eps.exceptions.*;
import org.socraticgrid.hl7.services.eps.interfaces.BrokerManagementIFace;
import org.socraticgrid.hl7.services.eps.interfaces.PublicationIFace;
import org.socraticgrid.hl7.services.eps.interfaces.SubscriptionIFace;
import org.socraticgrid.hl7.services.eps.model.*;
import org.socraticgrid.hl7.services.eps.publish.EPSBroker;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.util.*;

/**
 * Provides a Helper to publsh to a EPS broker The class not designed to allow a
 * different the endpoint or publisher to be changed in a multi-threaded
 * environment.
 * 
 * @author Jerry Goodnough
 *
 */
public class EPSBrokerDstu3 {

	private static final QName PUBLICATION_SERVICE_NAME = new QName("org.socraticgrid.hl7.services.eps",
			"PublicationServiceService");

	private static final QName BROKERMGMT_SERVICE_NAME = new QName("org.socraticgrid.hl7.services.eps",
			"BrokerManagementServiceService");

	private static final QName SUBSCRIPTION_SERVICE_NAME = new QName("org.socraticgrid.hl7.services.eps",
			"SubscriptionServiceService");

	private FhirContext fhirCtx;

	private boolean assignTimes = true;

	/**
	 * @return the assignTimes
	 */
	public boolean isAssignTimes() {
		return assignTimes;
	}

	/**
	 * @param assignTimes the assignTimes to set
	 */
	public void setAssignTimes(boolean assignTimes) {
		this.assignTimes = assignTimes;
	}

	
	public EPSBrokerDstu3()
	{
		fhirCtx = FhirContext.forDstu3();
	}

	/**
	 * Get the current FhirContext used to serialize resources
	 * 
	 * @return
	 */
	public FhirContext getFhirCtx() {
		return fhirCtx;
	}

	/**
	 * Allow the FhirContext to be overwritten.
	 * 
	 * @param fhirCtx
	 */
	public void setFhirCtx(FhirContext newfhirCtx) {
		this.fhirCtx = newfhirCtx;
	}

	private String brokerEndpoint;
	private User publisher;
	private PublicationIFace publisherPort;
	private BrokerManagementIFace brokerMgmtPort;
	private SubscriptionIFace subscriptionPort;

	protected void createEndpoints() {

		PublicationServiceSE ss = new PublicationServiceSE(PublicationServiceSE.WSDL_LOCATION, PUBLICATION_SERVICE_NAME);

		String endpoint = brokerEndpoint + "/publication";

		publisherPort = ss.getPublicationPort();
		((BindingProvider) publisherPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);

		BrokerManagementServiceSE ss1 = new BrokerManagementServiceSE(BrokerManagementServiceSE.WSDL_LOCATION, BROKERMGMT_SERVICE_NAME);

		endpoint = brokerEndpoint + "/brokermanagement";

		brokerMgmtPort = ss1.getBrokerManagementPort();
		((BindingProvider) brokerMgmtPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);


		SubscriptionServiceSE ss2 = new SubscriptionServiceSE(SubscriptionServiceSE.WSDL_LOCATION,SUBSCRIPTION_SERVICE_NAME);
		endpoint = brokerEndpoint + "/subscriptionService";

		subscriptionPort = ss2.getSubscriptionPort();
		((BindingProvider) subscriptionPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
	}

	/**
	 * Get the Root Endpoint of the Broker. The service prefixes are
	 * automatically added.
	 * 
	 * @return
	 */
	public String getBrokerEndpoint() {
		return brokerEndpoint;
	}

	public User getPublisher() {
		return publisher;
	}

	/**
	 * Create a Simpile open Transient Topic
	 *
	 * @param parentTopicName ("/" for root)
	 * @param topicName - Simple topic name with no "/". Nested creation NYI
	 * @return CreationResult
	 */
	public OperationStatus createSimpleTopic(String parentTopicName, String topicName)
	{
		OperationStatus out = new OperationStatus();
		Topic topic = new Topic();
		topic.setName(topicName);
		topic.setDescription("Simple topic");
		Options optionsList = new Options();
		optionsList.setAccess(AccessModel.Open);
		optionsList.setDurability(Durability.Transient);
		topic.setOptionsList(optionsList);
		try {
			CreationResult result = brokerMgmtPort.createTopic(parentTopicName, topicName, topic);
			if (result == CreationResult.Success)
			{
				out.setSuccess(true);
			}
			else
			{
				out.setSuccess(false);
				out.setFault(result.toString());
			}
		} catch (NotAuthorizedException | AuthenicationRequiredException | ConflictException | NoSuchTopicException
				| ExpiredException | FeatureNotAvailableException | InvalidDataException e) {
			out.setSuccess(false);
			out.setFault(e.getMessage());
			out.setExp(e);
		}
		return out;
	}

	/**
	 * Create a simple callback subscription to an open topic
	 * @param topic  The topic to subscribe to
	 * @param callbackAddress A valid endpoint for events to be delivered to
	 * @return The Subscription Id, Used to unsubscribe from the topic
	 * @throws MediaFormatNotExceptedException
	 * @throws IncompleteDataException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws ExpiredException
	 * @throws FeatureNotAvailableException
	 * @throws InvalidDataException
	 * @throws NoSuchTopicException
	 */
	public String subscribeToTopic(String topic, String callbackAddress ) throws MediaFormatNotExceptedException, IncompleteDataException, NotAuthorizedException, AuthenicationRequiredException, ExpiredException, FeatureNotAvailableException, InvalidDataException, NoSuchTopicException
	{
		List<String> topics = new LinkedList<String>();
		topics.add(topic);
		SubscriptionType type = SubscriptionType.Push;
		Options options = new Options();
		options.setAccess(AccessModel.Open);
		options.setDurability(Durability.Transient);

		String subscriptionId = subscriptionPort.subscribe(topics, type, options,
				callbackAddress);

		return subscriptionId;

	}


	/**
	 *
	 * @param topics List of topics to subscribe too.
	 * @param callbackAddress A valid endpoint for events to be delivered to
	 * @return The Subscription Id, Used to unsubscribe from the topic
	 * @throws MediaFormatNotExceptedException
	 * @throws IncompleteDataException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws ExpiredException
	 * @throws FeatureNotAvailableException
	 * @throws InvalidDataException
	 * @throws NoSuchTopicException
	 */
	public String subscribeToTopics(List<String> topics, String callbackAddress ) throws MediaFormatNotExceptedException, IncompleteDataException, NotAuthorizedException, AuthenicationRequiredException, ExpiredException, FeatureNotAvailableException, InvalidDataException, NoSuchTopicException
	{
		SubscriptionType type = SubscriptionType.Push;
		Options options = new Options();
		options.setAccess(AccessModel.Open);
		options.setDurability(Durability.Transient);

		String subscriptionId = subscriptionPort.subscribe(topics, type, options,
				callbackAddress);

		return subscriptionId;

	}

	/**
	 * Unsubscribe from a topic by subscription id
	 * @param topic
	 * @param subscriptionId
	 * @return
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws NoSuchItemException
	 * @throws InvalidDataException
	 */
	public boolean unsubscribeFromTopic(String topic, String subscriptionId) throws NotAuthorizedException, AuthenicationRequiredException, NoSuchTopicException, NoSuchItemException, InvalidDataException{
		List<String> topics = new LinkedList<String>();
		topics.add(topic);

		boolean result = subscriptionPort.unsubscribe(topics, publisher.getName(),
				subscriptionId);
		return result;

	}

	/**
	 * Unsunscribe from multiple topics by subscription id
	 * @param topics
	 * @param subscriptionId
	 * @return
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws NoSuchItemException
	 * @throws InvalidDataException
	 */
	public boolean unsubscribeFromTopics(List<String> topics, String subscriptionId) throws NotAuthorizedException, AuthenicationRequiredException, NoSuchTopicException, NoSuchItemException, InvalidDataException{

		boolean result = subscriptionPort.unsubscribe(topics, publisher.getName(),
				subscriptionId);
		return result;

	}

	/**
	 * Publish a JSON string on a topic
	 * 
	 * @param topic
	 * @param rsc
	 * @param subject
	 * @param title
	 * @return
	 * @throws PubSubException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishJSONTopic(String topic, String rsc, String subject, String title)
			throws PubSubException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {
		String msgId = "";
                Date now = new Date();

		Message event = new Message();
		MessageHeader header = event.getHeader();
		header.setSubject(subject);
                header.setMessageCreatedTime(now);
                header.setMessagePublicationTime(now);
		event.setTitle(title);

		if (publisher != null) {
			header.setPublisher(publisher);
		}
		// Not sure we need to generate an Id
		header.setMessageId(Long.toString(System.currentTimeMillis()));

		event.getTopics().add(topic);
		MessageBody body = new MessageBody();

		body.setType("application/json");
		body.setBody(rsc);
		event.getMessageBodies().add(body);

		msgId = publisherPort.publishEvent(topic, event);
		return msgId;
	}

	/**
	 * Raw message publication driven by topics declared in the message
	 * 
	 * @param event
	 * @return
	 * @throws IncompleteDataException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public Map<String, String> publishMessage(Message event)
			throws IncompleteDataException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {
		HashMap<String, String> msgIds = new HashMap<>();

		Iterator<String> itr = event.getTopics().iterator();
		while (itr.hasNext()) {
			String topic = itr.next();
			String msgId = publisherPort.publishEvent(topic, event);
			msgIds.put(topic, msgId);
		}
		return msgIds;
	}

	/**
	 * Raw Message publication
	 * 
	 * @param topic
	 * @param event
	 * @return
	 * @throws IncompleteDataException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishMessageToTopic(String topic, Message event)
			throws IncompleteDataException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {
		String msgId = "";
		if (topicNotInMessage(event, topic)) {
			event.getTopics().add(topic);
		}
		msgId = publisherPort.publishEvent(topic, event);
		return msgId;
	}

	/**
	 * Publish a FHIR resource to a topic (as JSON) using a default subject &
	 * title
	 * 
	 * @param topic
	 * @param rsc
	 * @return
	 * @throws PubSubException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishResourceToTopic(String topic, Resource rsc)
			throws PubSubException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {

		return this.publishResourceToTopic(topic, rsc, "FHIR Resource", rsc.getResourceType().name());

	}

	/**
	 * Publish a FHIR resource to a topic (as JSON) using a default subject &
	 * title
	 *
	 * @param topic
	 * @param rsc
	 * @return
	 * @throws PubSubException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishResourceIdToTopic(String topic, IIdType resourceId)
			throws PubSubException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {

		return this.publishResourceIdToTopic(topic, resourceId, "FHIR Resource", resourceId.getResourceType());

	}

	/**
	 * Publish a FHIR resouce to a topic, proving a title and subject
	 * 
	 * @param topic
	 * @param rsc
	 * @param subject
	 * @param title
	 * @return
	 * @throws PubSubException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishResourceToTopic(String topic, Resource rsc, String subject, String title)
			throws PubSubException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {
		String payload = fhirCtx.newJsonParser().encodeResourceToString(rsc);
		return publishPayloadToTopic(topic, "application/json+fhir", payload, subject, title);
	}

	/**
	 * Publish a FHIR resouce to a topic, proving a title and subject
	 *
	 * @param topic
	 * @param idType
	 * @param subject
	 * @param title
	 * @return
	 * @throws PubSubException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishResourceIdToTopic(String topic, IIdType idType, String subject, String title)
			throws PubSubException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {
		String payload = idType.getValueAsString();
		return publishPayloadToTopic(topic, "text/plain", payload, subject, title);
	}

	/**
	 * Publish a FHIR resouce to a topic, proving a title and subject
	 *
	 * @param topic
	 * @param mimeType
	 * @param payload
	 * @param subject
	 * @param title
	 * @return
	 * @throws PubSubException
	 * @throws MediaFormatNotExceptedException
	 * @throws NotAuthorizedException
	 * @throws AuthenicationRequiredException
	 * @throws NoSuchTopicException
	 * @throws InvalidDataException
	 */
	public String publishPayloadToTopic(String topic, String mimeType, String payload, String subject, String title)
			throws PubSubException, MediaFormatNotExceptedException, NotAuthorizedException,
			AuthenicationRequiredException, NoSuchTopicException, InvalidDataException {
		String msgId = "";
		Date now = new Date();
		Message event = new Message();
		MessageHeader header = event.getHeader();
		header.setSubject(subject);
		header.setTopicId(topic);
		header.setMessageCreatedTime(now);
		header.setMessagePublicationTime(now);
		event.setTitle(title);

		if (publisher != null) {
			header.setPublisher(publisher);
		}
		// Not sure we need to generate an Id
		header.setMessageId(Long.toString(System.currentTimeMillis()));

		event.getTopics().add(topic);
		MessageBody body = new MessageBody();

		body.setType(mimeType);
		body.setBody(payload);
		event.getMessageBodies().add(body);

		msgId = publisherPort.publishEvent(topic, event);
		return msgId;
	}

	/**
	 * Define the root endpoint for the broker. Should not be changed when the instance is used in a multi-threaded manner.
	 * 
	 * @param brokerEndpoint
	 */
	public void setBrokerEndpoint(String brokerEndpoint) {
		this.brokerEndpoint = brokerEndpoint;
		createEndpoints();
	}

	/**
	 * Set a publisher to use for all publications from this instance,
	 * Should not be changed when the instance is used in a multi-threaded manner.
	 * @param publisher
	 */
	public void setPublisher(User publisher) {
		this.publisher = publisher;
	}

	/**
	 * Scan the event/message to see if the topic is included
	 * 
	 * @param event
	 * @param topic
	 * @return
	 */
	protected boolean topicNotInMessage(Message event, String topic) {
		boolean out = true;
		Iterator<String> itr = event.getTopics().iterator();
		while (itr.hasNext()) {
			String chk = itr.next();
			if (chk.compareTo(topic) == 0) {
				out = false;
				break;
			}
		}
		return out;
	}

	public class OperationStatus
	{
		boolean success = false;
		Exception exp = null;
		String fault = null;
		/**
		 * @return the success
		 */
		public boolean isSuccess() {
			return success;
		}
		/**
		 * @param success the success to set
		 */
		public void setSuccess(boolean success) {
			this.success = success;
		}
		/**
		 * @return the exp
		 */
		public Exception getExp() {
			return exp;
		}
		/**
		 * @param exp the exp to set
		 */
		public void setExp(Exception exp) {
			this.exp = exp;
		}
		/**
		 * @return the fault
		 */
		public String getFault() {
			return fault;
		}
		/**
		 * @param fault the fault to set
		 */
		public void setFault(String fault) {
			this.fault = fault;
		}

	}

}
