package com.cognitivemecidine.hapi.eps.dstu3;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import org.hl7.fhir.dstu3.model.Enumerations;

/**
 * Created by cnanjo on 4/30/16.
 */
public class Context {

    private RestOperationTypeEnum operation;
    private PublicationScopeEnum publicationScope;
    private Enumerations.ResourceType resourceType;

    public Context() {}

    public Context(RestOperationTypeEnum operation, PublicationScopeEnum publicationScope, Enumerations.ResourceType resourceType) {
        this.operation = operation;
        this.publicationScope = publicationScope;
        this.resourceType = resourceType;
    }

    public RestOperationTypeEnum getOperation() {
        return operation;
    }

    public void setOperation(RestOperationTypeEnum operation) {
        this.operation = operation;
    }

    public PublicationScopeEnum getPublicationScope() {
        return publicationScope;
    }

    public void setPublicationScope(PublicationScopeEnum publicationScope) {
        this.publicationScope = publicationScope;
    }

    public Enumerations.ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(Enumerations.ResourceType resourceType) {
        this.resourceType = resourceType;
    }
}
