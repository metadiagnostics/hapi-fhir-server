package com.cognitivemecidine.hapi.eps.dstu3;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import java.util.Date;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.socraticgrid.hl7.services.eps.model.Message;
import org.socraticgrid.hl7.services.eps.model.MessageBody;
import org.socraticgrid.hl7.services.eps.model.MessageHeader;

/**
 * Created by esteban
 */
public class EPSRSResourcePublisheEPS implements IResourcePublisher {

    private static class RESTClient {

        public static RESTClient create(String endpoint) {

            Client client = ClientBuilder.newClient(new ClientConfig().register(com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider.class));
            WebTarget target = client.target(endpoint);

            RESTClient rc = new RESTClient();
            rc.setWebTarget(target);

            return rc;
        }

        private WebTarget webTarget;

        private RESTClient() {
        }

        private void setWebTarget(WebTarget webTarget) {
            this.webTarget = webTarget;
        }

        public WebTarget getWebTarget() {
            return webTarget;
        }

    }

    private final RESTClient epsClient;
    private final WebTarget webTarget;
    private final IParser parser;

    public EPSRSResourcePublisheEPS(String endpoint) {
        this.epsClient = RESTClient.create(endpoint);
        this.webTarget = this.epsClient.getWebTarget().path("rest").path("publication");
        this.parser = FhirContext.forDstu3().newJsonParser();
    }

    @Override
    public boolean meetsCriteria(Context context, Resource resource) {
        return true;
    }

    @Override
    public boolean meetsCriteria(Context context, IIdType resourceId) {
        return true;
    }

    @Override
    public void publishResource(Context context, Resource resource) {
        try {
            String topic = resolveTopic(resource.getResourceType().name());
            Message msg = this.createMessage(resource, topic);
            
            Response response = webTarget.path("topics").path(topic).request().post(Entity.json(msg));
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new RuntimeException("Response Status of the executed operation was " + response.getStatus());
            }

        } catch (Exception e) {
            throw new RuntimeException("Error publishing resource to EPS", e);
        }
    }

    @Override
    public void publishResourceId(Context context, IIdType id) {
        try {
            
            String resourceTypeStr = id.getResourceType();
            String topic = resolveTopic(resourceTypeStr);
            Message msg = this.createMessage(id, topic);
            
            Response response = webTarget.path("topics").path(topic).request().post(Entity.json(msg));
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new RuntimeException("Response Status of the executed operation was " + response.getStatus());
            }
        } catch (Exception e) {
            throw new RuntimeException("Error publishing resource to EPS", e);
        }
    }

    public String resolveTopic(String resourceType) {
        return "Patient";//TODO Fix when we know the topics we wish to publish to
    }
    
    private Message createMessage(IIdType idType, String topic){
        String title = idType.getResourceType();
        String subject = "FHIR Resource";
        String mimeType = "text/plain";
        String payload = idType.getValueAsString();
        
		Date now = new Date();
		Message event = new Message();
		MessageHeader header = event.getHeader();
		header.setSubject(subject);
		header.setTopicId(topic);
		header.setMessageCreatedTime(now);
		header.setMessagePublicationTime(now);
        
		event.setTitle(title);

		// Not sure we need to generate an Id
		header.setMessageId(Long.toString(System.currentTimeMillis()));

		event.getTopics().add(topic);
		MessageBody body = new MessageBody();

		body.setType(mimeType);
		body.setBody(payload);
		event.getMessageBodies().add(body);

		return event;
    }
    
    private Message createMessage(Resource rsc, String topic){
        String title = rsc.getResourceType().name();
        String subject = "FHIR Resource";
        String mimeType = "application/json+fhir";
        String payload = this.parser.encodeResourceToString(rsc);
        
		Date now = new Date();
		Message event = new Message();
		MessageHeader header = event.getHeader();
		header.setSubject(subject);
		header.setTopicId(topic);
		header.setMessageCreatedTime(now);
		header.setMessagePublicationTime(now);
        
		event.setTitle(title);

		// Not sure we need to generate an Id
		header.setMessageId(Long.toString(System.currentTimeMillis()));

		event.getTopics().add(topic);
		MessageBody body = new MessageBody();

		body.setType(mimeType);
		body.setBody(payload);
		event.getMessageBodies().add(body);

		return event;
    }
}
