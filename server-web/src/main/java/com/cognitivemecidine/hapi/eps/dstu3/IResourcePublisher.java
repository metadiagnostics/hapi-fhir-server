package com.cognitivemecidine.hapi.eps.dstu3;

import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 * Created by cnanjo on 4/30/16.
 */
public interface IResourcePublisher {
    public boolean meetsCriteria(Context context, Resource resource);
    public boolean meetsCriteria(Context context, IIdType resourceId);
    public void publishResource(Context context, Resource resource);
    public void publishResourceId(Context context, IIdType id);
}
