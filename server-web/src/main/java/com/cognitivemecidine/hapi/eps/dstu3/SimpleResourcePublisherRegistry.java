package com.cognitivemecidine.hapi.eps.dstu3;

import ca.uhn.fhir.jpa.entity.ResourceTable;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.server.interceptor.IServerInterceptor;
import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

import java.util.*;

/**
 * Created by cnanjo on 4/30/16.
 */
public class SimpleResourcePublisherRegistry implements IResourcePublisherRegistry {

    private Map<RestOperationTypeEnum, HashSet<IResourcePublisher>> restToPublisherIndex;
    private Map<Enumerations.ResourceType, HashSet<IResourcePublisher>> resourceToPublisherIndex;
    private HashSet<IResourcePublisher> resourcePublishers;

    public SimpleResourcePublisherRegistry() {
        restToPublisherIndex = new HashMap<>();
        resourceToPublisherIndex = new HashMap<>();
        resourcePublishers = new HashSet<>();
    }

    @Override
    public void registerResourcePublisher(Context context, IResourcePublisher publisher) {
        resourcePublishers.add(publisher);
        addPublisherToRestOperationBasedIndex(context, publisher);
        addPublisherToResourceBasedIndex(context, publisher);
    }

    public void clearAllPublishers() {
        this.restToPublisherIndex.clear();
        this.resourceToPublisherIndex.clear();
        resourcePublishers.clear();
    }

    public void addResourcePublisher(Context context, IResourcePublisher publisher) {
        resourcePublishers.add(publisher);
        addPublisherToRestOperationBasedIndex(context, publisher);
        addPublisherToResourceBasedIndex(context, publisher);
    }

    private void addPublisherToRestOperationBasedIndex(Context context,  IResourcePublisher publisher) {
        RestOperationTypeEnum operation = context.getOperation();
        if(operation != null) {
            HashSet<IResourcePublisher> publishers = restToPublisherIndex.get(operation);
            if(publishers == null) {
                publishers = new HashSet<>();
                restToPublisherIndex.put(operation, publishers);
            }
            publishers.add(publisher);
        }
    }

    private void addPublisherToResourceBasedIndex(Context context, IResourcePublisher publisher) {
        PublicationScopeEnum scope = context.getPublicationScope();
        Enumerations.ResourceType resourceType = context.getResourceType();
        if(scope != null) {
            if(scope == PublicationScopeEnum.ALL) {
                HashSet<IResourcePublisher> publishers = resourceToPublisherIndex.get(Enumerations.ResourceType.RESOURCE);
                if(publishers == null) {
                    publishers = new HashSet<>();
                    resourceToPublisherIndex.put(Enumerations.ResourceType.RESOURCE, publishers);
                }
                publishers.add(publisher);
            } else if(resourceType != null) {
                HashSet<IResourcePublisher> publishers = resourceToPublisherIndex.get(resourceType);
                if(publishers == null) {
                    publishers = new HashSet<>();
                    resourceToPublisherIndex.put(resourceType, publishers);
                }
                publishers.add(publisher);
            }
        }
    }

    public Set<IResourcePublisher> getFromOperationIndex(RestOperationTypeEnum operation) {
        HashSet<IResourcePublisher> publishers = restToPublisherIndex.get(operation);
        if(publishers == null) {
            publishers = new HashSet<IResourcePublisher>();
            restToPublisherIndex.put(operation, publishers);
        }
        return (Set<IResourcePublisher>)publishers.clone();
    }

    public Set<IResourcePublisher> getFromResourceToPublisherIndex(Enumerations.ResourceType scope) {
        HashSet<IResourcePublisher> publishers = resourceToPublisherIndex.get(scope);
        if(publishers == null) {
            publishers = new HashSet<IResourcePublisher>();
            resourceToPublisherIndex.put(scope, publishers);
        }
        return (Set<IResourcePublisher>)publishers.clone();
    }

    public Set<IResourcePublisher> getMatchingResourcePublishers(Context context) {
        PublicationScopeEnum scope = context.getPublicationScope();
        RestOperationTypeEnum operation = context.getOperation();
        Enumerations.ResourceType resourceType = context.getResourceType();
        if(scope == PublicationScopeEnum.ALL) {
            resourceType = Enumerations.ResourceType.RESOURCE;
        }
        Set<IResourcePublisher> forOperation = getFromOperationIndex(operation);
        Set<IResourcePublisher> forScope = getFromResourceToPublisherIndex(resourceType);
        forOperation.retainAll(forScope);
        return forOperation;
    }

    public boolean hasRegisteredPublishers() {
        return resourcePublishers.size() > 0;
    }

    public boolean hasRegisteredPublishers(RestOperationTypeEnum operation) {
        return restToPublisherIndex.get(operation) != null && restToPublisherIndex.get(operation).size() > 0;
    }

    public boolean hasRegisteredPublishAllPublishers(RestOperationTypeEnum operation) {
        return hasRegisteredAllResourcesPublishers() && hasRegisteredPublishers(operation);
    }

    public boolean hasRegisteredAllResourcesPublishers() {
        Set<IResourcePublisher> publishers = resourceToPublisherIndex.get(Enumerations.ResourceType.RESOURCE);
        return publishers != null && publishers.size() > 0;
    }

    public void triggerPublicationForAnyResources(IServerInterceptor.ActionRequestDetails theDetails, ResourceTable theResourceTable) {
        IBaseResource resource = theDetails.getResource();
        RestOperationTypeEnum operation = theDetails.getRequestDetails().getRestOperationType();
        if(hasRegisteredPublishers(operation)) {
            Context context = new Context(operation, PublicationScopeEnum.ALL, Enumerations.ResourceType.RESOURCE);
            if(resource == null) {
                publishResourceId(context, theDetails.getId());
            } else {
                publishResource(context, (Resource)resource);
            }

        }
    }

    public void publishResource(Context context, Resource resource) {
        Set<IResourcePublisher> publishers = getMatchingResourcePublishers(context);
        for(IResourcePublisher publisher : publishers) {
            if(publisher.meetsCriteria(context, resource)) {
                publisher.publishResource(context, resource);
            }
        }
    }

    public void publishResourceId(Context context, IIdType resourceId) {
        Set<IResourcePublisher> publishers = getMatchingResourcePublishers(context);
        for(IResourcePublisher publisher : publishers) {
            if(publisher.meetsCriteria(context, resourceId)) {
                publisher.publishResourceId(context, resourceId);
            }
        }
    }
}
