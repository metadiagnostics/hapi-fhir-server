package com.cognitivemecidine.hapi.eps.dstu3;

/**
 * Created by cnanjo on 4/30/16.
 */
public interface IResourcePublisherRegistry {
    public void registerResourcePublisher(Context context, IResourcePublisher publisher);
}
