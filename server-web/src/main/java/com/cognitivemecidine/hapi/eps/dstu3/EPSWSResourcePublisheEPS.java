package com.cognitivemecidine.hapi.eps.dstu3;

import com.cognitivemecidine.client.EPSBrokerDstu3;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 * Created by cnanjo on 5/1/16.
 */
public class EPSWSResourcePublisheEPS implements IResourcePublisher {

    private EPSBrokerDstu3 myEpsBroker;

    public EPSWSResourcePublisheEPS(EPSBrokerDstu3 epsBroker) {
        this.myEpsBroker = epsBroker;
    }

    @Override
    public boolean meetsCriteria(Context context, Resource resource) {
        return true;
    }

    @Override
    public boolean meetsCriteria(Context context, IIdType resourceId) {
        return true;
    }

    @Override
    public void publishResource(Context context, Resource resource) {
        try {
            myEpsBroker.publishResourceToTopic(resolveTopic(resource.getResourceType().name()), resource);
        } catch(Exception e) {
            throw new RuntimeException("Error publishing resource to EPS", e);
        }
    }

    @Override
    public void publishResourceId(Context context, IIdType id) {
        String resourceTypeStr = id.getResourceType();
        try {
            myEpsBroker.publishResourceIdToTopic(resolveTopic(resourceTypeStr), id);
        } catch(Exception e) {
            throw new RuntimeException("Error publishing resource to EPS", e);
        }
    }

    public String resolveTopic(String resourceType) {
        return "Patient";//TODO Fix when we know the topics we wish to publish to
    }
}
