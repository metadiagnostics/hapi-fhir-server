package ca.uhn.fhir.jpa.demo;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirSystemDao;
import ca.uhn.fhir.jpa.provider.JpaConformanceProviderDstu1;
import ca.uhn.fhir.jpa.provider.JpaConformanceProviderDstu2;
import ca.uhn.fhir.jpa.provider.JpaSystemProviderDstu1;
import ca.uhn.fhir.jpa.provider.JpaSystemProviderDstu2;
import ca.uhn.fhir.jpa.provider.dstu3.JpaConformanceProviderDstu3;
import ca.uhn.fhir.jpa.provider.dstu3.JpaSystemProviderDstu3;
import ca.uhn.fhir.jpa.search.DatabaseBackedPagingProvider;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.composite.MetaDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.narrative.DefaultThymeleafNarrativeGenerator;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.server.ETagSupportEnum;
import ca.uhn.fhir.rest.server.EncodingEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.IServerInterceptor;
import com.cognitivemecidine.client.EPSBrokerDstu3;
import com.cognitivemecidine.hapi.eps.dstu3.Context;
import com.cognitivemecidine.hapi.eps.dstu3.EPSRSResourcePublisheEPS;
import com.cognitivemecidine.hapi.eps.dstu3.PublicationScopeEnum;
import com.cognitivemecidine.hapi.eps.dstu3.IResourcePublisher;
import com.cognitivemecidine.hapi.eps.dstu3.SimpleResourcePublisherRegistry;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.hapi.config.ConfigConstants;
import com.cognitivemedicine.hapi.eps.EpsInterceptor;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.dstu3.model.Meta;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletException;
import java.util.Collection;
import java.util.List;

public class JpaServerDemo extends RestfulServer {

    private static final long serialVersionUID = 1L;

    private WebApplicationContext myAppCtx;

    @SuppressWarnings("unchecked")
    @Override
    protected void initialize() throws ServletException {
        super.initialize();

		/* 
         * We want to support FHIR DSTU2 format. This means that the server
		 * will use the DSTU2 bundle format and other DSTU2 encoding changes.
		 *
		 * If you want to use DSTU1 instead, change the following line, and change the 2 occurrences of dstu2 in web.xml to dstu1
		 */
        FhirVersionEnum fhirVersion = FhirVersionEnum.DSTU3;
        setFhirContext(new FhirContext(fhirVersion));

        // Get the spring context from the web container (it's declared in web.xml)
        myAppCtx = ContextLoaderListener.getCurrentWebApplicationContext();

		/* 
		 * The BaseJavaConfigDstu2.java class is a spring configuration
		 * file which is automatically generated as a part of hapi-fhir-jpaserver-base and
		 * contains bean definitions for a resource provider for each resource type
		 */
        String resourceProviderBeanName;
        if (fhirVersion == FhirVersionEnum.DSTU1) {
            resourceProviderBeanName = "myResourceProvidersDstu1";
        } else if (fhirVersion == FhirVersionEnum.DSTU2) {
            resourceProviderBeanName = "myResourceProvidersDstu2";
        } else if (fhirVersion == FhirVersionEnum.DSTU3) {
            resourceProviderBeanName = "myResourceProvidersDstu3_11";
        } else {
            throw new IllegalStateException();
        }
        List<IResourceProvider> beans = myAppCtx.getBean(resourceProviderBeanName, List.class);
        setResourceProviders(beans);
		
		/* 
		 * The system provider implements non-resource-type methods, such as
		 * transaction, and global history.
		 */
        Object systemProvider;
        if (fhirVersion == FhirVersionEnum.DSTU1) {
            systemProvider = myAppCtx.getBean("mySystemProviderDstu1", JpaSystemProviderDstu1.class);
        } else if (fhirVersion == FhirVersionEnum.DSTU2) {
            systemProvider = myAppCtx.getBean("mySystemProviderDstu2", JpaSystemProviderDstu2.class);
        } else if (fhirVersion == FhirVersionEnum.DSTU3) {
            systemProvider = myAppCtx.getBean(/*"mySystemProviderDstu3"*/ "myExtendedSystemProviderDstu3", JpaSystemProviderDstu3.class);
        } else {
            throw new IllegalStateException();
        }
        setPlainProviders(systemProvider);

		/*
		 * The conformance provider exports the supported resources, search parameters, etc for
		 * this server. The JPA version adds resource counts to the exported statement, so it
		 * is a nice addition.
		 */
        if (fhirVersion == FhirVersionEnum.DSTU1) {
            IFhirSystemDao<List<IResource>, MetaDt> systemDao = myAppCtx.getBean("mySystemDaoDstu1",
                    IFhirSystemDao.class);
            JpaConformanceProviderDstu1 confProvider = new JpaConformanceProviderDstu1(this, systemDao);
            confProvider.setImplementationDescription("Example Server");
            setServerConformanceProvider(confProvider);
        } else if (fhirVersion == FhirVersionEnum.DSTU2) {
            IFhirSystemDao<Bundle, MetaDt> systemDao = myAppCtx.getBean("mySystemDaoDstu2", IFhirSystemDao.class);
            JpaConformanceProviderDstu2 confProvider = new JpaConformanceProviderDstu2(this, systemDao,
                    myAppCtx.getBean(DaoConfig.class));
            confProvider.setImplementationDescription("Example Server");
            setServerConformanceProvider(confProvider);
        } else if (fhirVersion == FhirVersionEnum.DSTU3) {
            IFhirSystemDao<org.hl7.fhir.dstu3.model.Bundle, Meta> systemDao = myAppCtx
                    .getBean("mySystemDaoDstu3", IFhirSystemDao.class);
            SimpleResourcePublisherRegistry registry = myAppCtx.getBean("myResourcePublisherRegistry", SimpleResourcePublisherRegistry.class);
            DaoConfig daoConfig = myAppCtx.getBean(DaoConfig.class);
            EpsInterceptor interceptor = new EpsInterceptor();
            interceptor.setResourcePublisherRegistry(registry);
            daoConfig.setInterceptors(interceptor);
            JpaConformanceProviderDstu3 confProvider = new JpaConformanceProviderDstu3(this, systemDao,
                    daoConfig);
            confProvider.setImplementationDescription("Example Server");
            setServerConformanceProvider(confProvider);
        } else {
            throw new IllegalStateException();
        }

		/*
		 * Enable ETag Support (this is already the default)
		 */
        setETagSupport(ETagSupportEnum.ENABLED);

		/*
		 * This server tries to dynamically generate narratives
		 */
        FhirContext ctx = getFhirContext();
        ctx.setNarrativeGenerator(new DefaultThymeleafNarrativeGenerator());

		/*
		 * Default to JSON and pretty printing
		 */
        setDefaultPrettyPrint(true);
        setDefaultResponseEncoding(EncodingEnum.JSON);

		/*
		 * -- New in HAPI FHIR 1.5 --
		 * This configures the server to page search results to and from
		 * the database
		 */
        setPagingProvider(myAppCtx.getBean(DatabaseBackedPagingProvider.class));

		/*
		 * Load interceptors for the server from Spring (these are defined in FhirServerConfig.java)
		 */
        Collection<IServerInterceptor> interceptorBeans = myAppCtx.getBeansOfType(IServerInterceptor.class).values();
        for (IServerInterceptor interceptor : interceptorBeans) {
            this.registerInterceptor(interceptor);
        }

		/*
		 * If you are hosting this server at a specific DNS name, the server will try to 
		 * figure out the FHIR base URL based on what the web container tells it, but
		 * this doesn't always work. If you are setting links in your search bundles that
		 * just refer to "localhost", you might want to use a server address strategy:
		 */
        //setServerAddressStrategy(new HardcodedServerAddressStrategy("http://mydomain.com/fhir/baseDstu2"));

		/*
		 * Added by CN
		 */
        registerResourcePublishers();
    }

    /**
     * Added by CN:
     * Registering two default Resource Publishers:
     * 1. A publisher that publishes all newly created resources
     * 2. A publisher that publishes only Patient resources upon creation
     * Comment out or adjust as needed.
     */
    public void registerResourcePublishers() {
        try {

            SimpleResourcePublisherRegistry registry = myAppCtx.getBean("myResourcePublisherRegistry", SimpleResourcePublisherRegistry.class);
            EPSBrokerDstu3 epsBroker = myAppCtx.getBean("epsBroker", EPSBrokerDstu3.class);

            ConfigUtils cfg = ConfigUtils.getInstance(ConfigConstants.CONFIG_CONTEXT_NAME);
            boolean registerAllPublishers = cfg.getBoolean(ConfigConstants.KEY_EPS_REGISTER_ALL_PUBLISHERS);
            boolean registerResourceSpecificPublishers = cfg.getBoolean(ConfigConstants.KEY_EPS_REGISTER_RESOURCE_SPECIFIC_PUBLISHERS);

            String epsEndpoint = cfg.getString(ConfigConstants.KEY_EPS_ENDPOINT);
            
            if(registerAllPublishers) {
                //IResourcePublisher allCreatePublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher allCreatePublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.CREATE, PublicationScopeEnum.ALL, Enumerations.ResourceType.RESOURCE), allCreatePublisher);

                //IResourcePublisher allUpdatePublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher allUpdatePublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.UPDATE, PublicationScopeEnum.ALL, Enumerations.ResourceType.RESOURCE), allUpdatePublisher);

//                IResourcePublisher allDeletePublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher allDeletePublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.DELETE, PublicationScopeEnum.ALL, Enumerations.ResourceType.RESOURCE), allDeletePublisher);

//                IResourcePublisher allTransactionPublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher allTransactionPublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.TRANSACTION, PublicationScopeEnum.ALL, Enumerations.ResourceType.RESOURCE), allTransactionPublisher);
            }

            if(registerResourceSpecificPublishers) {
//                IResourcePublisher createPatientPublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher createPatientPublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.CREATE, PublicationScopeEnum.RESOURCE, Enumerations.ResourceType.PATIENT), createPatientPublisher);

//                IResourcePublisher updatePatientPublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher updatePatientPublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.UPDATE, PublicationScopeEnum.RESOURCE, Enumerations.ResourceType.PATIENT), updatePatientPublisher);

//                IResourcePublisher deletePatientPublisher = new EPSWSResourcePublisheEPS(epsBroker);
                IResourcePublisher deletePatientPublisher = new EPSRSResourcePublisheEPS(epsEndpoint);
                registry.registerResourcePublisher(new Context(RestOperationTypeEnum.DELETE, PublicationScopeEnum.RESOURCE, Enumerations.ResourceType.PATIENT), deletePatientPublisher);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //TODO See how to best handle
        }
    }

}
