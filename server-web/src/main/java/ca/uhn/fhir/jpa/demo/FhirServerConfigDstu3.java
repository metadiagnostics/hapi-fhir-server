package ca.uhn.fhir.jpa.demo;

import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cognitivemecidine.client.EPSBrokerDstu3;
import com.cognitivemecidine.hapi.eps.dstu3.SimpleResourcePublisherRegistry;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.hapi.config.ConfigConstants;
import com.cognitivemedicine.hapi.rp.HSPCDecisionSupportServiceModuleResourceProviderDstu3;

import ca.uhn.fhir.jpa.config.BaseJavaConfigDstu3;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.util.SubscriptionsRequireManualActivationInterceptorDstu3;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.interceptor.IServerInterceptor;
import ca.uhn.fhir.rest.server.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import com.cognitivemedicine.hapi.security.BasicSecurityInterceptor;

/**
 * This class isn't used by default by the example, but you can use it as a config if you want to
 * support DSTU3 instead of DSTU2 in your server.
 * <p>
 * See https://github.com/jamesagnew/hapi-fhir/issues/278
 */
@Configuration
@EnableTransactionManagement()
public class FhirServerConfigDstu3 extends BaseJavaConfigDstu3 {
    
    /**
     * Configure FHIR properties around the the JPA server via this bean
     */
    @Bean()
    public DaoConfig daoConfig() {
        DaoConfig retVal = new DaoConfig();
        retVal.setSubscriptionEnabled(true);
        retVal.setSubscriptionPollDelay(5000);
        retVal.setSubscriptionPurgeInactiveAfterMillis(DateUtils.MILLIS_PER_HOUR);
        retVal.setAllowMultipleDelete(true);
        return retVal;
    }
    
    /**
     * The following bean configures the database connection. The 'url' property value of
     * "jdbc:derby:directory:jpaserver_derby_files;create=true" indicates that the server should
     * save resources in a directory called "jpaserver_derby_files".
     * <p>
     * A URL to a remote database could also be placed here, along with login credentials and other
     * properties supported by BasicDataSource.
     */
    @Bean(destroyMethod = "close")
    public DataSource dataSource() {
        BasicDataSource retVal = new BasicDataSource();
        retVal.setDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        retVal.setUrl("jdbc:derby:directory:target/jpaserver_derby_files;create=true");
        retVal.setUsername("");
        retVal.setPassword("");
        return retVal;
    }
    
    @Bean()
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean retVal = new LocalContainerEntityManagerFactoryBean();
        retVal.setPersistenceUnitName("HAPI_PU");
        retVal.setDataSource(dataSource());
        retVal.setPackagesToScan("ca.uhn.fhir.jpa.entity");
        retVal.setPersistenceProvider(new HibernatePersistenceProvider());
        retVal.setJpaProperties(jpaProperties());
        return retVal;
    }
    
    private Properties jpaProperties() {
        Properties extraProperties = new Properties();
        extraProperties.put("hibernate.dialect", org.hibernate.dialect.DerbyTenSevenDialect.class.getName());
        extraProperties.put("hibernate.format_sql", "true");
        extraProperties.put("hibernate.show_sql", "false");
        extraProperties.put("hibernate.hbm2ddl.auto", "update");
        extraProperties.put("hibernate.jdbc.batch_size", "20");
        extraProperties.put("hibernate.cache.use_query_cache", "false");
        extraProperties.put("hibernate.cache.use_second_level_cache", "false");
        extraProperties.put("hibernate.cache.use_structured_entries", "false");
        extraProperties.put("hibernate.cache.use_minimal_puts", "false");
        extraProperties.put("hibernate.search.default.directory_provider", "filesystem");
        extraProperties.put("hibernate.search.default.indexBase", "target/lucenefiles");
        extraProperties.put("hibernate.search.lucene_version", "LUCENE_CURRENT");
        //		extraProperties.put("hibernate.search.default.worker.execution", "async");
        return extraProperties;
    }
    
    /**
     * Do some fancy logging to create a nice access log that has details about each incoming
     * request.
     */
    public IServerInterceptor loggingInterceptor() {
        LoggingInterceptor retVal = new LoggingInterceptor();
        retVal.setLoggerName("fhirtest.access");
        retVal.setMessageFormat(
            "Path[${servletPath}] Source[${requestHeader.x-forwarded-for}] Operation[${operationType} ${operationName} ${idOrResourceName}] UA[${requestHeader.user-agent}] Params[${requestParameters}] ResponseEncoding[${responseEncodingNoDefault}]");
        retVal.setLogExceptions(true);
        retVal.setErrorMessageFormat("ERROR - ${requestVerb} ${requestUrl}");
        return retVal;
    }
    
    /**
     * This interceptor adds some pretty syntax highlighting in responses when a browser is detected
     */
    @Bean(autowire = Autowire.BY_TYPE)
    public IServerInterceptor responseHighlighterInterceptor() {
        ResponseHighlighterInterceptor retVal = new ResponseHighlighterInterceptor();
        return retVal;
    }
    
    @Bean(autowire = Autowire.BY_TYPE)
    public IServerInterceptor subscriptionSecurityInterceptor() {
        SubscriptionsRequireManualActivationInterceptorDstu3 retVal = new SubscriptionsRequireManualActivationInterceptorDstu3();
        return retVal;
    }
    
    @Bean(autowire = Autowire.BY_TYPE, name = "basicSecurityInterceptor")
    public IServerInterceptor basicSecurityInterceptor() {
        return new BasicSecurityInterceptor();
    }

    @Bean()
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager retVal = new JpaTransactionManager();
        retVal.setEntityManagerFactory(entityManagerFactory);
        return retVal;
    }
    
    @Override
    @Bean(name = "myResourceProvidersDstu3_11")
    public List<IResourceProvider> resourceProvidersDstu3() {
        List<IResourceProvider> retVal = super.resourceProvidersDstu3();
        return retVal;
    }
    
    @Override
    @Bean(name = "myPatientRpDstu3")
    @Lazy
    public ca.uhn.fhir.jpa.rp.dstu3.PatientResourceProvider rpPatientDstu3() {
        com.cognitivemedicine.hapi.rp.HSPCPatientResourceProvider retVal;
        retVal = new com.cognitivemedicine.hapi.rp.HSPCPatientResourceProvider();
        retVal.setContext(fhirContextDstu3());
        retVal.setDao(daoPatientDstu3());
        return retVal;
    }

    
    @Override
    @Bean(name = "myDecisionSupportRuleRpDstu3")
    @Lazy
    public HSPCDecisionSupportServiceModuleResourceProviderDstu3 rpDecisionSupportServiceModuleDstu3() {
        HSPCDecisionSupportServiceModuleResourceProviderDstu3 retVal;
        retVal = new HSPCDecisionSupportServiceModuleResourceProviderDstu3();
        retVal.setContext(fhirContextDstu3());
        retVal.setDao(daoDecisionSupportServiceModuleDstu3());
        return retVal;
    }
    
    @Bean(name = "epsBroker")
    public EPSBrokerDstu3 epsBroker() {
        
        ConfigUtils cfg = ConfigUtils.getInstance(ConfigConstants.CONFIG_CONTEXT_NAME);
        String epsEndpoint = cfg.getString(ConfigConstants.KEY_EPS_ENDPOINT);
        
        EPSBrokerDstu3 broker = null;
        try {
            broker = new EPSBrokerDstu3();
            broker.setBrokerEndpoint(epsEndpoint);
        } catch (Exception e) {
            e.printStackTrace();
            //TODO See how to best handle
        }
        return broker;
    }
    
    @Bean(name = "myExtendedSystemProviderDstu3")
    public com.cognitivemedicine.hapi.rp.ExtendedJpaSystemProviderDstu3 extendedSystemProviderDstu3() {
        com.cognitivemedicine.hapi.rp.ExtendedJpaSystemProviderDstu3 retVal = new com.cognitivemedicine.hapi.rp.ExtendedJpaSystemProviderDstu3();
        retVal.setDao(systemDaoDstu3());
        return retVal;
    }
    
    @Bean(name = "myResourcePublisherRegistry")
    public SimpleResourcePublisherRegistry resourcePublisherRegistry() {
        return new SimpleResourcePublisherRegistry();
    }
    
}
