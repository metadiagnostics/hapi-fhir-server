package ca.uhn.fhir.jpa.demo;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IIdType;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ExampleServerIT {

	private static IGenericClient ourClient;
	private static final FhirContext ourCtx = FhirContext.forDstu3();
	private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(ExampleServerIT.class);

	private static int ourPort;

	private static Server ourServer;
	private static String ourServerBase;

	@Test
	public void testCreateAndRead() throws IOException {
		String methodName = "testCreateResourceConditional";

		Patient pt = new Patient();
		pt.addName().addFamily("TestPatient1");
		IIdType id = ourClient.create().resource(pt).execute().getId();
//
//		Patient pt2 = ourClient.read().resource(Patient.class).withId(id).execute();
//		assertEquals(methodName, pt2.getName().get(0).getFamily().get(0).getValue());

		Bundle results = ourClient
				.search()
				.forResource(Patient.class)
				.where(Patient.FAMILY.matches().value("duck0000"))
				.returnBundle(Bundle.class)
				.execute();
	}

	@AfterClass
	public static void afterClass() throws Exception {
//		ourServer.stop();
	}

	@BeforeClass
	public static void beforeClass() throws Exception {
		/*
		 * This runs under maven, and I'm not sure how else to figure out the target directory from code..
		 */
//		String path = ExampleServerIT.class.getClassLoader().getResource(".keep_hapi-fhir-jpaserver-example").getPath();
//		path = new File(path).getParent();
//		path = new File(path).getParent();
//		path = new File(path).getParent();
//
//		ourLog.info("Project base path is: {}", path);
//
//		ourPort = RandomServerPortProvider.findFreePort();
//		ourServer = new Server(ourPort);
//
//		WebAppContext webAppContext = new WebAppContext();
//		webAppContext.setContextPath("/");
//		webAppContext.setDescriptor(path + "/src/main/webapp/WEB-INF/web.xml");
//		webAppContext.setResourceBase(path + "/target/hapi-fhir-jpaserver-example");
//		webAppContext.setParentLoaderPriority(true);
//
//		ourServer.setHandler(webAppContext);
//		ourServer.start();
//
//		ourCtx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
//		ourCtx.getRestfulClientFactory().setSocketTimeout(1200 * 1000);
		//ourServerBase = "http://localhost:" + ourPort + "/baseDstu2";
		ourServerBase = "http://localhost:8080/hapi/baseDstu3";
		ourClient = ourCtx.newRestfulGenericClient(ourServerBase);
		ourClient.registerInterceptor(new LoggingInterceptor(true));

	}

}
